<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>EXERCICI 1 - TAULES MULTIPLICAR</title>
    </head>
    <body>
       
        <h1>EXERCICI 1 - TAULA MULTIPLICAR</h1>
        <form method="post" action="index.jsp">
            Quina taula de multiplicar mostro ? <input type="number" name="numero" />
            <input type="submit" name="ok" value="Enviar"/>
        </form>  
          <%
            //si ha clicat o no al botó del formulari
           if(request.getParameter("ok")!=null) {
              String value = request.getParameter("numero");
              // Pendent controlar error si no posa un número
              int numero = Integer.parseInt(value);
          %>
          <ul>
              <%
              for(int i = 0; i <= 10; i ++) { 
              %>
                <li><%=numero * i %></li>
              <%
                }
            }
           %>
          </ul>
      
    </body>
</html>
